__author__ = 'sage'
from Classes import Character
import random

class Gnome(Character.Character):

    nickname = ""

    def __init__(self, name="Gnomish name"):
        super(Gnome, self).__init__(name)
        self.generate()

        if random.randint(1, 10) >= 7:
            self.nickname = "'" + self.random("Nicknames") + "'"

    def list_output(self):
        returnStr = ""
        returnStr += "Name:       " + self.name + " " + self.surname + "\n" if self.nickname == "" else \
                     "Name:       " + self.name + " "  + self.nickname + " "+ self.surname + "\n"
        returnStr += "Gender:     " + self.gender + "\n"
        returnStr += "Race:       " + self.race + "\n"
        returnStr += "Alignment:  " + self.alignment + "\n"
        returnStr += "Ideal:      " + self.ideal + "\n"
        returnStr += "Appearance: " + self.appearance + "\n"
        returnStr += "Talent:     " + self.talent + "\n"
        returnStr += "Mannerism:  " + self.mannerism + "\n"
        returnStr += "Bond:       " + self.bond + "\n"
        returnStr += "Flaw:       " + self.flaw + "\n"
        return returnStr


