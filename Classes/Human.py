from Classes import Character

__author__ = 'sage'


class Human(Character.Character):

    def __init__(self, name="Human name", output_mode="List"):
        super(Human, self).__init__(name)
        self.generate()
