__author__ = 'sage'

import json
import random

class Character(object):

    output_mode = "List"
    json_data = ""
    json_data_global = ""

    name = ""
    surname = ""
    gender = ""
    race = ""
    alignment = ""
    appearance = ""
    talent = ""
    mannerism = ""
    interactions = ""
    bond = ""
    ideal = ""
    flaw = ""

    def __init__(self, name="", gender="Male"):
        self.name = name

        with open("i18n/en/" + self.__class__.__name__ + ".json") as json_file:
            self.json_data = json.load(json_file)

        with open("i18n/en/global.json") as json_file:
            self.json_data_global = json.load(json_file)

        self.gender = self.random("Gender")
        self.race = self.__class__.__name__
        self.alignment = self.random("Alignments")
        self.ideal = self.random(self.alignment + "Ideals")
        self.appearance = self.random("NPCAppearance")
        self.talent = self.random("NPCTalent")
        self.mannerism = self.random("Mannerisms")
        self.bond = self.random("NPCBond")
        self.flaw = self.random("NPCFlaw")

    def generate(self):
        self.surname = self.random("Surnames")
        self.name = self.random(self.gender + "Names")

    def random(self, key=""):
        try:
            rnd = random.choice(self.json_data[key])
        except KeyError:
            try:
                rnd = random.choice(self.json_data_global[key])
            except KeyError:
                rnd = ""
        return rnd

    def list_output(self):
        returnStr = ""
        returnStr += "Name:       " + self.name + " " + self.surname + "\n"
        returnStr += "Gender:     " + self.gender + "\n"
        returnStr += "Race:       " + self.race + "\n"
        returnStr += "Alignment:  " + self.alignment + "\n"
        returnStr += "Ideal:      " + self.ideal + "\n"
        returnStr += "Appearance: " + self.appearance + "\n"
        returnStr += "Talent:     " + self.talent + "\n"
        returnStr += "Mannerism:  " + self.mannerism + "\n"
        returnStr += "Bond:       " + self.bond + "\n"
        returnStr += "Flaw:       " + self.flaw + "\n"

        return returnStr

    def __str__(self):
        if self.output_mode == "List":
            return self.list_output()
        else:
            return "{0} {1} is a {2} " + self.race  + ".".format(self.name, self.surname, self.gender)
