__author__ = 'sage'
from Classes import Character

import json


class HalfElf(Character.Character):

    def __init__(self, name="Halfelvish name"):
        super(HalfElf, self).__init__(name)
        self.generate()

    def generate(self):
        nametype = self.random("NameTypes")

        self.gender = self.random("Gender")

        if nametype == "Elf name with elf surname":
            with open("i18n/en/Elf.json") as json_file:
                self.json_data = json.load(json_file)

            self.name = self.random(self.gender + "Names")
            self.surname = self.random("Surnames")
        elif nametype == "Elf name with human surname":
            with open("i18n/en/Elf.json") as json_file:
                self.json_data = json.load(json_file)
            self.name = self.random(self.gender + "Names")

            with open("i18n/en/Human.json") as json_file:
                self.json_data = json.load(json_file)
            self.surname = self.random("Surnames")
        elif nametype == "Human name with human surname":
            with open("i18n/en/Human.json") as json_file:
                self.json_data = json.load(json_file)

            self.name = self.random(self.gender + "Names")
            self.surname = self.random("Surnames")
        elif nametype == "Human name with elf surname":
            with open("i18n/en/Human.json") as json_file:
                self.json_data = json.load(json_file)
            self.name = self.random(self.gender + "Names")

            with open("i18n/en/Elf.json") as json_file:
                self.json_data = json.load(json_file)
            self.surname = self.random("Surnames")
