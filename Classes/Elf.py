__author__ = 'sage'
from Classes import Character


class Elf(Character.Character):

    def __init__(self, name="Elven name"):
        super(Elf, self).__init__(name)
        self.generate()
