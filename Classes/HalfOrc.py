__author__ = 'sage'
from Classes import Character
import json


class HalfOrc(Character.Character):

    def __init__(self, name="HalfOrc name"):
        #Character.__init__(name)
        super(HalfOrc, self).__init__(name)
        self.generate()

    def generate(self):
        nametype = self.random("NameTypes")

        self.gender = self.random("Gender")

        if nametype == "Orc name with surname":
            with open("i18n/en/Orc.json") as json_file:
                self.json_data = json.load(json_file)

            self.name = self.random(self.gender + "Names")

            with open("i18n/en/Human.json") as json_file:
                self.json_data = json.load(json_file)
            self.surname = self.random("Surnames")
        elif nametype == "Orc name without surname":
            with open("i18n/en/Orc.json") as json_file:
                self.json_data = json.load(json_file)
            self.name = self.random(self.gender + "Names")

            self.surname = ""
        elif nametype == "Human name":
            with open("i18n/en/Human.json") as json_file:
                self.json_data = json.load(json_file)

            self.name = self.random(self.gender + "Names")
            self.surname = self.random("Surnames")
