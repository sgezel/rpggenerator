__author__ = 'sage'

import json
import random
import importlib


class Generator:

    json_data = ""
    output_mode = ""

    def __init__(self, output="List"):
        with open("i18n/en/global.json") as json_file:
            self.json_data = json.load(json_file)
        self.output_mode = output

    def generatenpc(self):
        race = random.choice(self.json_data["Races"])
        self.generate(race)

    def generate(self, race="Human", output_mode="list"):
        module = importlib.import_module("Classes." + race)
        npc = getattr(module, race)(output_mode)
        print npc
